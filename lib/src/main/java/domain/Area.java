package domain;

import java.util.ArrayList;
import java.util.List;


public class Area {
	private String name;
	private List<Device> devices;
		
	public Area(String name) {
		this.name=name;
		this.devices=new ArrayList<>();				
	}
	
	public void addDevice(Device device) {
		this.devices.add(device);
	}

	public String getName() {
		return name;
	}

	public List<Device> getDevices() {
		return devices;
	}
	

	
	
}
