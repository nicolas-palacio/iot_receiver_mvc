package domain;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;

public class Receiver implements Observable<ReceiverObserver>{
	private List<ReceiverObserver>observers;
	private Path path;	 
	
	public Receiver(Path path) {
		this.path=path;
		this.observers=new ArrayList<>();
		
	}
	
	public void startListen() {
		WatchService watchService = FileSystems.getDefault().newWatchService();
		
		//Register the directory for specific events
		this.path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
		
		 while (true) {
             WatchKey key = watchService.take();

             for (WatchEvent<?> event : key.pollEvents()){
                 // Handle the specific event
                 if (event.kind() == StandardWatchEventKinds.ENTRY_CREATE){
                     System.out.println("File created: " + event.context());
                     receive((File)event.context());
                 } 
             }            
             key.reset();
         }catch (IOException | InterruptedException e) {
             e.printStackTrace();
         }

     } 
		
	
	public void receive(File reading) {
		// TODO Check reading and notify Observer
		notifyObservers();		
	}

	@Override
	public void addObserver(ReceiverObserver t) {
		this.observers.add(t);
		
	}

	@Override
	public void removeObserver(ReceiverObserver t) {
		// TODO Auto-generated method stub
		
	}

	private void notifyObservers() {
		for(ReceiverObserver observer:this.observers) {
			observer.notify();
		}
	}
	
	

}
