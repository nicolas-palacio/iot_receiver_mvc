package domain;

import java.util.ArrayList;
import java.util.List;

import domain.enums.DeviceType;

public class Device {
	private DeviceType type;
	private String name;
	private List<Double> readings;
	
	
	public Device(String name,DeviceType type) {
		this.name=name;
		this.type=type;
		this.readings=new ArrayList<>();
	}
	
	
	public void generateReadings() {
		//TODO write readings
	}


	public DeviceType getType() {
		return type;
	}


	public String getName() {
		return name;
	}
	
	

}
