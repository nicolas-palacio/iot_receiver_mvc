package domain;


public interface ReceiverObserver {
	void update(String eventType, String reading);

}
