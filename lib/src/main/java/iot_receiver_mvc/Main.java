package iot_receiver_mvc;

import java.nio.file.Paths;
import controller.Controller;
import domain.Area;
import domain.Device;
import domain.Receiver;
import domain.enums.DeviceType;
import view.View;

public class Main {

	public static void main(String[] args) {	

		Receiver receiver=new Receiver(Paths.get("./"));
		Device thermometer= new Device("Thermometer1",DeviceType.TEMPERATURE);		
		Area classroom=new Area("classroom");		
		classroom.addDevice(thermometer);
		
		
		Controller controller=new Controller(classroom,receiver);
		View window = new View(controller);
		receiver.addObserver(window);
		
		receiver.startListen();
		thermometer.generateReadings();
		window.initialize();
		
	}

}
