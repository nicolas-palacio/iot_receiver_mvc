package view;

import java.awt.EventQueue;

import javax.swing.JFrame;

import controller.Controller;
import domain.ReceiverObserver;

public class View implements ReceiverObserver{

	private JFrame frame;
	private Controller controller;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View window = new View();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the application.
	 */
	public View(Controller controller) {
		this.controller=controller;
		//initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frame = new JFrame();
		frame.setVisible(true);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void update(String eventType, String reading) {
		// TODO Update device reading
		
	}

}
