package controller;

import java.util.List;



import domain.Area;
import domain.Device;
import domain.Receiver;

public class Controller {
	private Area area;
	private Receiver receiver;

	public Controller(Area area, Receiver receiver) {
		this.area=area;
		this.receiver=receiver;
		
	}
	
	
	public List<Device> getDevices(String areaName){
		return area.getDevices();
	}
	
}
